var lista_clientes = [];

function CrearListaCliente(cedula, nombre, apellido, fecha_nacimiento, direccion) {

    var Cliente = {
        cedula: cedula,
        nombre: nombre,
        apellido: apellido,
        fecha_nacimiento: fecha_nacimiento,
        direccion: direccion
    }
    lista_clientes.push(Cliente);

}

function ObtenerListaClientes() {

    var listaClientes = localStorage.getItem('ListaClientes');
    if (listaClientes == null) {
        lista_clientes = [];
    } else {
        lista_clientes = JSON.parse(listaClientes);
    }
    return lista_clientes;
}