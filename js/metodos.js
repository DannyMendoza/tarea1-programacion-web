function botonFormulario() {

    btnAgg = document.getElementById("btn-agg").value;

    if (btnAgg == "Registrar cliente") {
        aggCliente();

    } else {

        editarCliente();
    }

}


function aggCliente() {

    var cedula = document.getElementById("cedula").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var fecha = document.getElementById("fecha").value;
    var direccion = document.getElementById("direccion").value;


    if (cedula.length == 0 || nombre.length == 0 || apellido.length == 0 || fecha.length == 0 || direccion.length == 0) {
        error();
    } else {


        CrearListaCliente(cedula, nombre, apellido, fecha, direccion)

        localStorage.setItem('ListaClientes', JSON.stringify(lista_clientes));

        obtenerClientes();
        success();

        document.getElementById("cedula").value = "";
        document.getElementById("nombre").value = "";
        document.getElementById("apellido").value = "";
        document.getElementById("fecha").value = "";
        document.getElementById("direccion").value = "";

    }
}



function obtenerClientes() {

    var list = ObtenerListaClientes(),
        tbody = document.querySelector('#tabla-clientes tbody');

    tbody.innerHTML = '';
    for (let i = 0; i < list.length; i++) {
        var row = tbody.insertRow(i),

            cedula = row.insertCell(0),
            nombre = row.insertCell(1),            
            apellido = row.insertCell(2),
            fecha = row.insertCell(3),
            direccion = row.insertCell(4),
            acciones = row.insertCell(5);

        cedula.innerHTML = list[i].cedula;
        nombre.innerHTML = list[i].nombre;        
        apellido.innerHTML = list[i].apellido;
        fecha.innerHTML = list[i].fecha_nacimiento;
        direccion.innerHTML = list[i].direccion;
        acciones.innerHTML = '<button type="buttton" class="btn-borrar" onclick="eliminar(' + i + ')"> <i class="far fa-trash-alt"></i> </button> <button type="buttton" class="btn-editar" onclick="obtenerClienteEditar(' + i + ')"> <i class="far fa-edit"></i> </button>';


        tbody.appendChild(row);
    }
}



function obtenerClienteEditar(idCliente) {

    localStorage.setItem('idCliente', idCliente);

    var list = ObtenerListaClientes();

    document.getElementById("cedula").value = list[idCliente].cedula;
    document.getElementById("nombre").value = list[idCliente].nombre;
    document.getElementById("apellido").value = list[idCliente].apellido;
    document.getElementById("fecha").value = list[idCliente].fecha_nacimiento;
    document.getElementById("direccion").value = list[idCliente].direccion;

    document.getElementById("direccion").value = list[idCliente].direccion;

    btnCancelar = document.getElementById("btn-cancelar");
    btnCancelar.style.display = "block";

    btnAgg = document.getElementById("btn-agg").value = "Editar";

    var containerForm = document.getElementById("container-form");
    containerForm.scrollIntoView();


}

function editarCliente() {

    var cedula = document.getElementById("cedula").value;
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var fecha = document.getElementById("fecha").value;
    var direccion = document.getElementById("direccion").value;


    if (cedula.length == 0 || nombre.length == 0 || apellido.length == 0 || fecha.length == 0 || direccion.length == 0) {
        error();
    } else {

        var list = ObtenerListaClientes();

        var idCliente = localStorage.getItem('idCliente');

        var cedula = document.getElementById("cedula").value;
        var nombre = document.getElementById("nombre").value;
        var apellido = document.getElementById("apellido").value;
        var fecha = document.getElementById("fecha").value;
        var direccion = document.getElementById("direccion").value;

        list[idCliente].cedula = cedula;
        list[idCliente].nombre = nombre;
        list[idCliente].apellido = apellido;
        list[idCliente].fecha_nacimiento = fecha;
        list[idCliente].direccion = direccion;

        list.splice(idCliente, 1, list[idCliente]);

        localStorage.setItem('ListaClientes', JSON.stringify(list));

        obtenerClientes();
        success();
        cancelar();
    }


}

function cancelar() {

    document.getElementById("cedula").value = "";
    document.getElementById("nombre").value = "";
    document.getElementById("apellido").value = "";
    document.getElementById("fecha").value = "";
    document.getElementById("direccion").value = "";

    btnCancelar = document.getElementById("btn-cancelar");
    btnCancelar.style.display = "none";
    btnAgg = document.getElementById("btn-agg").value = "Registrar cliente";

}



function success() {
    Swal.fire({
        title: 'Correcto',
        text: 'Registro exitoso',
        icon: 'success',
        showConfirmButton: false,
        timer: 1000
    })
}

function error() {
    Swal.fire({
        title: 'Error',
        text: 'Ingrese todos los datos',
        icon: 'error',
        showConfirmButton: false,
        timer: 1000
    })
}


function borrarCliente(idCliente) {

    var list = ObtenerListaClientes();
    list.splice(idCliente, 1);
    localStorage.setItem('ListaClientes', JSON.stringify(list));
    obtenerClientes();

}


function eliminar(idCliente) {
    Swal.fire({
        title: '¿Eliminar este registro?',
        text: "Este registro se eliminará",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'

    }).then((result) => {
        if (result.isConfirmed) {

            borrarCliente(idCliente);

            Swal.fire(
                'Correcto',
                'Su registro ha sido eliminado',
                'success'
            )
        }
    })
}